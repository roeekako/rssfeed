$(document).ready(function () { 
   	$.getJSON("newsfeed.json", function(result) {
   		$.each(result, function(key, val) {
   			if ( val.image != "" ) {
   				$(".container").append("<div class='list-element col-md-6 col-lg-4 col-xs-12' data-link='" + val.link + "''><img src='"+ val.image +"'><h3>" + val.title + "</h3><p class='word-break'>" + val.description + "</p></div>" );
   			} else {
   				$(".container").append("<div class='list-element col-md-6 col-lg-4 col-xs-12' data-link='" + val.link + "''><h3>" + val.title + "</h3><p class='word-break'>" + val.description + "</p></div>" );
   			}
   			
   			$(".list-element:last-of-type").prepend("<div><span class='date'>" + val.pubDate + "</span></div><br>");
   			
   		});
   /*		$.get("https://www.forbes.com/sites/louiscolumbus/2018/04/08/the-state-of-cloud-business-intelligence-2018/#47bf4c572180",
   			{
   				'Content-Type': 'application/json',
   				'x-api-key':'Oy1zITEzp39Xqf570oHrM4BePG3XS203buTRVHB7'
   			}, function(data, status){
	        console.log("Data: " + data + "\nStatus: " + status);
	    });*/
   	});

   	$("body").on("click", ".list-element", function(evt) { 
   		var link = $(this).data("link"); console.log(link);
   		$.post('process.php', {link: link}, function(response) {
   			var obj = $.parseJSON(response);
			console.log($.type(obj));
			$.each(JSON.parse(obj), function(key, val) {
				
				if (key == "content") {
					console.log(val);
					$(".content-body").html(val);
					$(".close-button").addClass("show");
					$("#popup").show();
					$(".list-element").addClass("grey");
					$(".list-element img").hide();
				}
			}); 
		});
   	});

   	$(".close-button").on("click", function(evt) { console.log("evt: " + evt);
   		$(".content-body").html();
   		$(".close-button").removeClass("show");
   		$("#popup").hide();
   		$(".list-element").removeClass("grey");
   		$(".list-element img").show();
   	});


});