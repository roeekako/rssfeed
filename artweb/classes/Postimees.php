<?php
namespace test;

class Postimees extends Curl {
	const CONTAINER = '.list-article';
	const LINK      = 'a[class=list-article__url]';
	const IMG_URL   = 'a div[class=list-article__image]';
	const TITLE     = 'div[class=headline__content] h1[class=headline__title]';
	const ATTRIBUTE = 'data-lazy-src';

	public function __construct($url, $cacheFile) {
		parent::__construct($url, $cacheFile);
	}

	public function getArticleData($html) {

		$html->load_file($this->cacheFile);
		$div_array = $html->find(self::CONTAINER);
		$div_array = array_splice($div_array, 0, 12);
		$i = 0;

		foreach($div_array as $element) {
			

			foreach($element->find(self::LINK) as $a) {
				$link = $a->getAttribute('href');
			}
			foreach($element->find(self::IMG_URL) as $img) { 
				$url = $img->getAttribute(self::ATTRIBUTE);
				if ($url == '') {
					$url = $img->getAttribute('style');
					$url = str_replace("background-image: url('//", "", $url);
					$url = str_replace("')", "", $url);
				}
			}
			foreach($element->find('a[class=list-article__url] span[class=list-article__headline]') as $text) {
				$title = $text->plaintext;
			}
			$data[$i] = array('title' => $title,
							  'link'  => $link,
							  'url'   => $url,
							);
			$i++;
		}
		return $data;

	}


}

?>