<?php
namespace test;

class Delfi extends Curl {
	const CONTAINER = '.headline';
	const LINK      = 'a[class=headline__media]';
	const IMG_URL   = 'a figure img[class=headline__image]';
	const TITLE     = 'div[class=headline__content] h1[class=headline__title]';
	const ATTRIBUTE = 'src';

	public function __construct($url, $cacheFile) {
		parent::__construct($url, $cacheFile);
	}

	public function getArticleData($html) {

		$html->load_file($this->cacheFile);
		$div_array = $html->find(self::CONTAINER);
		$div_array = array_splice($div_array, 0, 12);
		$i = 0;

		foreach($div_array as $element) {

			foreach($element->find(self::LINK) as $href) {
				$link = $href->getAttribute('href');
			}
			foreach($element->find(self::IMG_URL) as $img) { 
				$url = $img->getAttribute(self::ATTRIBUTE);
			}
			foreach($element->find(self::TITLE) as $text) {
				$title = $text->plaintext;
			}
			$data[$i] = array('title' => $title,
							  'link'  => $link,
							  'url'   => $url,
							);
			$i++;
		}
		return $data;
	}
}
	




?>