<?php
namespace test;

class Curl {
	public $url;
	public $cacheFile;

	public function __construct($url, $cacheFile) {
		$url =  $this->url = $url;
		$cacheFile = $this->cacheFile = $cacheFile;
	}

	public function storeData() {
		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$html_data = curl_exec($ch);
		curl_close($ch);
		file_put_contents($this->cacheFile, $html_data);
	}
	
}
	


?>