<?php
require_once('includes/simple_html_dom.php');
require 'classes/Curl.php';
require 'classes/Postimees.php';
require 'classes/Delfi.php';
require 'classes/DelfiFactory.php';
require 'classes/PostimeesFactory.php';

use test\PostimeesFactory as Postimees;
use test\DelfiFactory as Delfi;

if (isset($_POST['filter']) && $_POST['filter'] == 'postimees') { 
	$url = 'https://www.postimees.ee';
	$cacheFile = "cache/postimees.html";
	$postimees = Postimees::create($url, $cacheFile);
	$postimees->storeData();
	$html = new simple_html_dom();
	$data = $postimees->getArticleData($html);
	echo json_encode($data, JSON_UNESCAPED_UNICODE);
}

if (isset($_POST['filter']) && $_POST['filter'] == 'delfi') { 
	$url = 'https://www.delfi.ee';
	$cacheFile = "cache/delfi.html";
	$delfi = Delfi::create($url, $cacheFile);
	$delfi->storeData();
	$html = new simple_html_dom();
	$data = $delfi->getArticleData($html);
	echo json_encode($data, JSON_UNESCAPED_UNICODE);
}

?>