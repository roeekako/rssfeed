<?php
namespace PostgreSQL;

class Insert {
	public $pdo;


	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	public function insertData($firstName, $lastName, $occupation, $fileName, $type, $filePath) {
        $sql = 'INSERT INTO users (first_name,last_name,occupation_code,application,file_name,type) VALUES(:firstName,:lastName,:occupation,:application,:fileName,:type)';
        try {
            $this->pdo->beginTransaction();

	        // create large object
	        $fileData = $this->pdo->pgsqlLOBCreate();
	        $stream = $this->pdo->pgsqlLOBOpen($fileData, 'w');
	            
	        // read data from the file and copy the stream
	        $fh = fopen($filePath, 'rb');
	        stream_copy_to_stream($fh, $stream);

	        $fh = null;
	        $stream = null;

	        $stmt = $this->pdo->prepare($sql);
	       
	        $stmt->execute([
	        	':firstName'    => $firstName,
	        	':lastName'     => $lastName,
	        	':occupation'   => $occupation,
	        	':application'  =>$fileData,
	        	':fileName'     => $fileName,
	        	':type'         => $type
	        ]);
	        $this->pdo->commit();

         } catch (\Exception $e) {
            $this->pdo->rollBack();
            throw $e;
        }
        return $this->pdo->lastInsertId('users_id_seq');
    }
}