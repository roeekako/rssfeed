<?php
 
namespace PostgreSQL;
 
/**
 * PostgreSQL PHP Update Demo
 */
class Update {
 
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
 
    public function update($firstName, $lastName, $occupation) {
 
        $sql = 'UPDATE users '
                . 'SET first_name = :firstName, '
                . 'last_name = :lastName, '
                . 'occupation_code = :occupation '
                . 'WHERE user_id = (select MAX(user_id) FROM users)';
 
        $stmt = $this->pdo->prepare($sql);
 
        // bind values to the statement
        $stmt->bindValue(':firstName', $firstName);
        $stmt->bindValue(':lastName', $lastName);
        $stmt->bindValue(':occupation', $occupation);

        // update data in the database
        $stmt->execute();
 
        // return the number of row affected
        return $stmt->rowCount();
    }
}