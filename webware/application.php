<?php //include 'process.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <title>Proovitöö</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="col-md-4 col-sm-6 col-xs-12 center">
          <form method="post" enctype="multipart/form-data" name="form" novalidate>
            <div class="form-group row">
              <label for="firstName" class="col-sm-2 col-form-label firstName">Firstname:</label>
              <div class="col-sm-6">
                <input type='text' class="form-control" name="firstName" required/>
              </div>
            </div>
            <div class="form-group row">
              <label for="lastName" class="col-sm-2 col-form-label lastName">Lastname:</label>
              <div class="col-sm-6">
                <input type='text' class="form-control" name="lastName" required/>
              </div>
            </div>
            <div class="form-group row">
              <label for="occupation" class="col-sm-2 col-form-label">Occupation:</label>
              <div class="col-sm-6">
                <select class="form-control" name="occupation">
                  <option>choose</option>
                </select>
              </div>
            </div>
            <div class="custom-file form-group row">
              <label class="custom-file-label col-sm-2" for="customFile">Application:</label>
              <div class="col-sm-6" id="fileContainer">
                <input type="file" class="custom-file-input" id="file" required>
                <a href="#">download here</a>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-8">
                <input type="submit" value="Submit" id="submitForm">
                <input type="submit" value="Empty form" id="emptyForm">
                <input type="submit" value="Update data" id="updateData">
              </div>
            </div>
          </form>
          <div class="error"></div>
        </div>
      </div>
    </div>
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/main.js"></script>
</body>
</html>