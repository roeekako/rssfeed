var filter;

$(document).ready(function() { 
  //check sessionStorage
   if (sessionStorage.firstName && sessionStorage.lastName && sessionStorage.occupation) { 
      filter = "file";
      $.post("process.php", {filter: filter}, function(response) {
          var file = response.replace(/\"/g, ""); 
          sessionStorage.fileName = file;
      });
      $("input[type='submit']").hide();
      $("input[value='Update data'], input[value='Empty form']").show();
      $("#file").hide();
      $("#fileContainer a").attr("href", "upload/" + sessionStorage.fileName) .show();
      $('input[name="firstName"]').val(sessionStorage.firstName); 
      $('input[name="lastName"]').val(sessionStorage.lastName);
   }
//get occupation data and set select options    
  var filter = 'getSelectOptions'; 
  $.post('process.php',{ filter: filter }, function(result) { 
      $.each(JSON.parse(result), function(k, val) { 
        if (val.name != '' && val.code != '') {
          if (val.code == sessionStorage.occupation) {
            $("select[name='occupation']").append("<option value='" + val.code + "' selected>" + val.name + "<option/>");
          } else {
            $("select[name='occupation']").append("<option value='" + val.code + "'>" + val.name + "<option/>");
          }
        }

      });
      //Temporary anomaly fix
      $("option").each(function() {
        if ($(this).val() == '')
          $(this).hide();
      });
  });
 
  //load file to folder
  $('input[type=file]').on('change', fileUpload);

  //submit form
  $("#submitForm").click(function() { 
    filter = 'insert';
    var firstName  = $('input[name="firstName"]').val(); 
    var lastName   = $('input[name="lastName"]').val();
    var occupation = $( "select option:selected" ).val(); 
    var file = $('#file').get(0).files[0]; 
    var fileName = file.name; 
    var type =file.type;

    //store form data in sessionStorage
    sessionStorage.firstName  = firstName;
    sessionStorage.lastName   = lastName;
    sessionStorage.occupation = occupation;
    sessionStorage.fileName   = fileName;
    sessionStorage.type       = type;

    if (occupation == 'choose') {
       $(".error").html("You missed to select occupation.").show();
       $("select").css("border", "2px solid rgb(128, 183, 225)");
    } else { 
      if($(".error").is(":visible") == true) {
          $(".error").hide();
          $("input").removeAttr("style");
      } 

       $.post('process.php',
        {
          filter:     filter,
          firstName:  firstName,
          lastName:   lastName,
          occupation: occupation,
          fileName:   fileName,
          type:       type
        });
    } // end else
  }); // end submit form
   
  //Update user data
  $("#updateData").click(function(event) {
      //kontrolli kas vormis on andmed muutunud ja saada ära muutunud andmed
      filter = 'update';
      var firstName  = $('input[name="firstName"]').val(); 
      var lastName   = $('input[name="lastName"]').val();
      var occupation = $( "select option:selected" ).val(); 
      sessionStorage.firstName  = firstName;
      sessionStorage.lastName   = lastName;
      sessionStorage.occupation = occupation;   

        $.post( "process",{
          filter:     filter,
          firstName:  firstName,
          lastName:   lastName,
          occupation: occupation
        });
       
  });
  //empty form and clear storage data
  $("#emptyForm").click(function() {
      sessionStorage.clear();

      //empty upload folder
      filter = 'emptyFolder';
      $.post('process.php',{ filter: filter }, function(result) { 
          //alert(result);
      });
      window.location.reload();
  });

});

function fileUpload(event){

    files = event.target.files; 
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        file = files[i]; console.log(file);
         if(!file.type.match('text.*|application.*')) {              
            //check file type
            $(".error").append("<p>Please choose some text or application file.</p>").show();

        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $(".error p").remove();
                }else if(response.status == 'type_err'){
                    $(".error").html("Some problem occured while uploading file, please try again.");
                }
            };
        }
    }
}


