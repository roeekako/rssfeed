
$(document).ready(function() {
	$.getJSON("https://erply-challenge.herokuapp.com/list?AUTH=fae7b9f6-6363-45a1-a9c9-3def2dae206d", function(result) { console.log(result);
		$("body").append("<table id ='json-table' class='table table-striped table-bordered'><thead><tr>"+
			                        "<th>Code</th>"+
									"<th>Department</th>"+
									"<th>Name</th>"+
									"<th>Image</th>"+
									"<th>Description</th>"+
									"<th>Price</th>"+
									"<th>Store</th>"+
									"<th>InS</th></tr></thead><tbody></tbody></table>");
		$.each(result, function(key, val) {
			var image = "<img src='"+ val.image +"' class='img-thumb'>"; 
			var color = (val.instock == true) ? "green" : "red"; 
			var price;
			if (val.currency == "EUR") {
				price = "€" + val.price;
			} else {
				price = val.price + ' ' +val.currency;
			}
			$("table").append("<tr><td>"+val.productcode+"</td>"+
								  "<td>"+val.department+"</td>"+
								  "<td>"+val.name+"</td>"+
								  "<td>"+ image +"</td>"+
								  "<td>"+val.description+"</td>"+
								  "<td>"+ price +"</td>"+
								  "<td>"+val.store+"</td>"+
								  "<td class='center'><span class='dot "+ color +"'></span><p>"+ val.instock +"</p></td></tr>");
		});
		$('table').dataTable({
		  paginate: false,
		  "order": [[ 7, "desc" ], [ 6, "desc" ]]
		});
	});
	
});