import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { IProduct } from '../product';
import { ProductService } from '../product.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductListComponent implements OnInit {
  pageTitle: string = "Products";
  errorMessage: string;
  locations:string[] = [];
  _listFilter: string;
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value:string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  filteredProducts: IProduct[];
  products: IProduct[] = [];

  constructor( private _productService: ProductService) { 
   }
   //get items in location
  performFilter(filterBy: string): IProduct[] { 
    return this.products.filter((product: IProduct) =>
      product.store.indexOf(filterBy) !== -1);
  }
  //get options to location select
  findLocation() {
    for (var i=0; i<this.products.length; i++) { 
      this.locations.push(this.products[i].store);
    }
  }

  ngOnInit() { 
    this._productService.getProducts()
          .subscribe(products => {
            this.products = products,
            this.filteredProducts = this.products;
            this.findLocation();
          }, 
           error => this.errorMessage = <any>error);
  }
  

}
