import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { IProduct } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _productUrl = 'https://erply-challenge.herokuapp.com/list?AUTH=fae7b9f6-6363-45a1-a9c9-3def2dae206d';
  
  constructor(private _http: HttpClient) { }

    getProducts(): Observable<IProduct[]> {
      return this._http.get<IProduct[]>(this._productUrl);
                      // .do(data => console.log('All: ' + JSON.stringify(data)))
                      // .catch(this.handleError);
    }

    getProduct(id: number): Observable<IProduct> {
      return this.getProducts()
          .map((products: IProduct[]) => products.find(p => p.id === id));
    }

    private handleError(err: HttpErrorResponse) {
      let errorMessage = '';
      if (err.error instanceof Error) {
          errorMessage = `An error occurred: ${err.error.message}`;
      } else {
          errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
      }
      console.error(errorMessage);
      return Observable.throw(errorMessage);
  }
  
}
